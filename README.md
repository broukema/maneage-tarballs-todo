Tarballs of free-licensed packages that may not yet be in Maneage

Compilation by B. Roukema 2022; this file itself is (C) 2022 CC BY.

This repository contains tarballs of free-licensed packages
(see the packages for the particular licences) that are
used in some Maneage projects and may not yet be included
in the main servers distributing these tarballs.

The file sha512sums contains checksums of the files; however,
you should trust the sha512sums in your Maneage source 
configuration file 'reproduce/software/config/checksums.conf'
rather than the sha512sums here.

See https://maneage.org , or for a backup of 
core maneage: https://codeberg.org/boud/maneage_dev .
